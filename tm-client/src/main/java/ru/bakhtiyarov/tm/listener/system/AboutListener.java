package ru.bakhtiyarov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;

@Component
public final class AboutListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    @EventListener(condition = "@aboutListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ruslan Bakhtiyarov");
        System.out.println("E-MAIL: rusya.vay@mail.ru");
    }

}
