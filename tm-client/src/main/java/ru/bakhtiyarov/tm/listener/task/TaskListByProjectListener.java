package ru.bakhtiyarov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.event.ConsoleEvent;
import ru.bakhtiyarov.tm.listener.AbstractListener;
import ru.bakhtiyarov.tm.endpoint.SessionDTO;
import ru.bakhtiyarov.tm.endpoint.TaskDTO;
import ru.bakhtiyarov.tm.endpoint.TaskEndpoint;
import ru.bakhtiyarov.tm.util.TerminalUtil;

import java.util.List;

@Component
public class TaskListByProjectListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "tasks-list-by-project-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list by project name.";
    }

    @EventListener(condition = "@taskListByProjectListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LIST TASKS]");
        System.out.println("ENTER PROJECT NAME:");
        final String projectName = TerminalUtil.nextLine();
        @NotNull SessionDTO session = sessionService.getSession();
        final List<TaskDTO> tasks = taskEndpoint.findAllByUserIdAndProjectName(session, projectName);
        int index = 1;
        for (@NotNull final TaskDTO task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}