package ru.bakhtiyarov.tm.service;

import org.junit.experimental.categories.Category;
import ru.bakhtiyarov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public class UserDTOServiceTest {

 /*   @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final UserDTO userDTO = new UserDTO("login", "password", "email");

    @Before
    public void addUser() {
        userRepository.add(userDTO);
    }

    @After
    public void removeUser() {
        userRepository.clearAll();
    }

    @Test
    public void testRemoveByLogin() {
        Assert.assertEquals(1, userService.findAll().size());
        userService.removeByLogin(userDTO.getLogin());
        Assert.assertEquals(0, userService.findAll().size());
    }

    @Test
    public void testCreateLoginPasswordEmail() {
        Assert.assertEquals(1, userService.findAll().size());
        UserDTO newUserDTO = userService.create("login", "password", "email");
        Assert.assertEquals(2, userService.findAll().size());
        Assert.assertNotNull(newUserDTO);
        Assert.assertEquals(newUserDTO.getLogin(), "login");
        Assert.assertEquals(newUserDTO.getEmail(), "email");
        Assert.assertEquals(newUserDTO.getRole(), Role.USER);
    }

    @Test
    public void testCreateLoginPassword() {
        Assert.assertEquals(1, userService.findAll().size());
        UserDTO newUserDTO = userService.create("login", "password");
        Assert.assertNotNull(newUserDTO);
        Assert.assertEquals(2, userService.findAll().size());
        Assert.assertEquals(newUserDTO.getLogin(), "login");
        Assert.assertEquals(newUserDTO.getRole(), Role.USER);
    }

    @Test
    public void testCreateLoginPasswordRole() {
        Assert.assertEquals(1, userService.findAll().size());
        UserDTO userDTO = userService.create("login", "password", Role.ADMIN);
        Assert.assertNotNull(userDTO);
        Assert.assertEquals(2, userService.findAll().size());
        Assert.assertEquals(userDTO.getLogin(), "login");
        Assert.assertEquals(userDTO.getRole(), Role.ADMIN);
    }

    @Test
    public void testUpdatePassword() {
        UserDTO nullUserDTO = userService.updatePassword(ConstTest.FALSE_USER_ID, "new pass");
        Assert.assertNull(nullUserDTO);
        UserDTO testUserDTO = userService.updatePassword(userDTO.getId(), "new pass");
        Assert.assertNotNull(testUserDTO);
        Assert.assertEquals(HashUtil.salt("new pass"), userDTO.getPasswordHash());
    }

    @Test
    public void testUpdateUserEmail() {
        UserDTO nullUserDTO = userService.updateUserEmail(ConstTest.FALSE_USER_ID, "email");
        Assert.assertNull(nullUserDTO);
        UserDTO testUserDTO = userService.updateUserEmail(userDTO.getId(), "new email");
        Assert.assertNotNull(testUserDTO);
        Assert.assertEquals(userDTO.getEmail(), "new email");
    }

    @Test
    public void testUpdateUserLogin() {
        UserDTO nullUserDTO = userService.updateUserLogin(ConstTest.FALSE_USER_ID, "login");
        Assert.assertNull(nullUserDTO);
        UserDTO testUserDTO = userService.updateUserLogin(userDTO.getId(), "new login");
        Assert.assertNotNull(testUserDTO);
        Assert.assertEquals(userDTO.getLogin(), "new login");
    }

    @Test
    public void testUpdateFirstName() {
        UserDTO nullUserDTO = userService.updateUserFirstName(ConstTest.FALSE_USER_ID, "first name");
        Assert.assertNull(nullUserDTO);
        UserDTO testUserDTO = userService.updateUserFirstName(userDTO.getId(), "new first name");
        Assert.assertNotNull(testUserDTO);
        Assert.assertEquals(userDTO.getFirstName(), "new first name");
    }

    @Test
    public void testUpdateLastName() {
        UserDTO nullUserDTO = userService.updateUserLastName(ConstTest.FALSE_USER_ID, "last name");
        Assert.assertNull(nullUserDTO);
        UserDTO testUserDTO = userService.updateUserLastName(userDTO.getId(), "new last name");
        Assert.assertNotNull(testUserDTO);
        Assert.assertEquals(userDTO.getLastName(), "new last name");
    }

    @Test
    public void testUpdateMiddleName() {
        UserDTO nullUserDTO = userService.updateUserMiddleName(ConstTest.FALSE_USER_ID, "mid name");
        Assert.assertNull(nullUserDTO);
        UserDTO testUserDTO = userService.updateUserMiddleName(userDTO.getId(), "new middle name");
        Assert.assertNotNull(testUserDTO);
        Assert.assertEquals(userDTO.getMiddleName(), "new middle name");
    }

    @Test
    public void testFindByLogin() {
        UserDTO testUserDTO = userService.findByLogin(userDTO.getLogin());
        Assert.assertNotNull(testUserDTO);
        Assert.assertEquals(testUserDTO.getId(), userDTO.getId());
        Assert.assertEquals(testUserDTO.getLogin(), userDTO.getLogin());
    }

    @Test
    public void testLockUserByLogin() {
        Assert.assertEquals(userDTO.getLocked(), false);
        userService.lockUserByLogin(userDTO.getLogin());
        Assert.assertEquals(userDTO.getLocked(), true);
    }

    @Test
    public void testUnLockedUserByLogin() {
        userService.lockUserByLogin(userDTO.getLogin());
        Assert.assertEquals(userDTO.getLocked(), true);
        userService.unLockUserByLogin(userDTO.getLogin());
        Assert.assertEquals(userDTO.getLocked(), false);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeRemoveWithEmptyLogin() {
        userService.removeByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeRemoveWithNullLogin() {
        userService.removeByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithEmailWithEmptyLogin() {
        userService.create("", "password", "email");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithEmailWithNullLogin() {
        userService.create(null, "password", "email");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithEmailWithEmptyPassword() {
        userService.create("login", "", "email");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithEmailWithNullPassword() {
        userService.create("login", null, "email");
    }

    @Test(expected = EmptyEmailException.class)
    public void testNegativeCreateWithEmailWithEmptyEmail() {
        userService.create("login", "password", "");
    }

    @Test(expected = EmptyEmailException.class)
    public void testNegativeCreateWithEmailWithNullEmail() {
        userService.create("login", "password", (String) null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithEmptyLogin() {
        userService.create("", "password");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithNullLogin() {
        userService.create(null, "password");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithEmptyPassword() {
        userService.create("login", "");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithNullPassword() {
        userService.create("login", null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithRoleWithEmptyLogin() {
        userService.create("", "password", Role.USER);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeCreateWithRoleWithNullLogin() {
        userService.create(null, "password", Role.USER);
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithRoleWithEmptyPassword() {
        userService.create("login", "", Role.USER);
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeCreateWithRoleWithNullPassword() {
        userService.create("login", null, Role.USER);
    }

    @Test(expected = EmptyRoleException.class)
    public void testNegativeCreateWithRoleWithNullRole() {
        userService.create("login", "password", (Role) null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdatePasswordWithEmptyId() {
        userService.updatePassword("", "password");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdatePasswordWithNullId() {
        userService.updatePassword(null, "password");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeUpdatePasswordWithEmptyPassword() {
        userService.updatePassword("id", "");
    }

    @Test(expected = EmptyPasswordException.class)
    public void testNegativeUpdatePasswordWithNullPassword() {
        userService.updatePassword("id", null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateEmailWithEmptyId() {
        userService.updateUserEmail("", "email");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateEmailWithNullId() {
        userService.updateUserEmail(null, "email");
    }

    @Test(expected = EmptyEmailException.class)
    public void testNegativeUpdateEmailWithEmptyEmail() {
        userService.updateUserEmail("id", "");
    }

    @Test(expected = EmptyEmailException.class)
    public void testNegativeUpdateEmailWithNullEmail() {
        userService.updateUserEmail("id", null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateFirstNameWithEmptyId() {
        userService.updateUserFirstName("", "first name");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateFirstNameWithNullId() {
        userService.updateUserFirstName(null, "first name");
    }

    @Test(expected = EmptyFirstNameException.class)
    public void testNegativeUpdateFirstNameWithEmptyFirstName() {
        userService.updateUserFirstName("id", "");
    }

    @Test(expected = EmptyFirstNameException.class)
    public void testNegativeUpdateFirstNameWithNullFirstName() {
        userService.updateUserFirstName("id", null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateLastNameWithEmptyId() {
        userService.updateUserLastName("", "last name");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateLastNameWithNullId() {
        userService.updateUserLastName(null, "last name");
    }

    @Test(expected = EmptyLastNameException.class)
    public void testNegativeUpdateLastNameWithEmptyLastName() {
        userService.updateUserLastName("id", "");
    }

    @Test(expected = EmptyLastNameException.class)
    public void testNegativeUpdateLastNameWithNullLastName() {
        userService.updateUserLastName("id", null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateMiddleNameWithEmptyId() {
        userService.updateUserMiddleName("", "middle name");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateMiddleNameWithNullId() {
        userService.updateUserMiddleName(null, "middle name");
    }

    @Test(expected = EmptyMiddleNameException.class)
    public void testNegativeUpdateMiddleNameWithEmptyLastName() {
        userService.updateUserMiddleName("id", "");
    }

    @Test(expected = EmptyMiddleNameException.class)
    public void testNegativeUpdateMiddleNameWithNullLastName() {
        userService.updateUserMiddleName("id", null);
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateLoginWithEmptyId() {
        userService.updateUserLogin("", "login");
    }

    @Test(expected = EmptyIdException.class)
    public void testNegativeUpdateLoginWithNullId() {
        userService.updateUserLogin(null, "login");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeUpdateLoginWithEmptyLastName() {
        userService.updateUserLogin("id", "");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeUpdateLoginWithNullLastName() {
        userService.updateUserLogin("id", null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeLockUserByLoginWithEmptyLogin() {
        userService.lockUserByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeLockUserByLoginWithNullLogin() {
        userService.lockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeUnLockUserByLoginWithEmptyLogin() {
        userService.unLockUserByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeUnLockUserByLoginWithNullLogin() {
        userService.unLockUserByLogin(null);
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeFindByLoginWithEmptyLogin() {
        userService.findByLogin("");
    }

    @Test(expected = EmptyLoginException.class)
    public void testNegativeFindUserByLoginWithNullLogin() {
        userService.findByLogin(null);
    }
*/
}