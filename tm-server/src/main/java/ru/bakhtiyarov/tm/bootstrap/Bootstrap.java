package ru.bakhtiyarov.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.api.service.IPropertyService;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.endpoint.AbstractEndpoint;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.xml.ws.Endpoint;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] abstractEndpoints;

    public void run() {
        initEndpoint();
        initUsers();
        System.out.println("SERVER START!!!");
    }

    private void initEndpoint() {
        for (@NotNull final AbstractEndpoint abstractEndpoint: abstractEndpoints)
        registryEndpoint(abstractEndpoint);
    }

    private void initUsers(){
        try {
            userService.create("test", "test", "test@mail.ru");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        try {
            userService.create("admin", "admin", Role.ADMIN);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void registryEndpoint(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}